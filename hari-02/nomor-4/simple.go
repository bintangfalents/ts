package main

import (

	"fmt"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	fmt.Println("Coba MySQL")

	db, err := sql.Open("mysql", "root:0000@tcp(127.0.0.1:3306)/cobadb")

	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	fmt.Println("Berhasil Konek ke MySQL database")

}